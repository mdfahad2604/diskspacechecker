// Mock data for demonstration
const cpuChartData = [30, 40, 30];
const memoryChartData = [20, 60, 20];
const driveChartData = [50, 30, 20];

document.addEventListener('DOMContentLoaded', function() {
  const homePage = document.getElementById('homePage');
  const systemStats = document.getElementById('systemStats');
  const cpuChart = document.getElementById('cpuChart');
  const memoryChart = document.getElementById('memoryChart');
  const driveChart = document.getElementById('driveChart');

  // Function to show home page and hide system stats
  function showHomePage() {
    homePage.style.display = 'block';
    systemStats.style.display = 'none';
  }

  // Function to show system stats and hide home page
  function showSystemStats() {
    homePage.style.display = 'none';
    systemStats.style.display = 'block';
  }

  // Function to display CPU chart
  function displayCPUChart() {
    // Replace this with your actual chart rendering logic
    cpuChart.innerHTML = '<p>CPU Utilization Chart</p>';
  }

  // Function to display Memory chart
  function displayMemoryChart() {
    // Replace this with your actual chart rendering logic
    memoryChart.innerHTML = '<p>Memory Utilization Chart</p>';
  }

  // Function to display Drive space chart
  function displayDriveChart() {
    // Replace this with your actual chart rendering logic
    driveChart.innerHTML = '<p>Drive Space Chart</p>';
  }

  // Event listeners for navigation
  document.querySelector('nav ul li:nth-child(1) a').addEventListener('click', function(event) {
    event.preventDefault();
    showHomePage();
  });

  document.querySelector('.dropdown-content a:nth-child(1)').addEventListener('click', function(event) {
    event.preventDefault();
    showSystemStats();
    displayCPUChart();
  });

  document.querySelector('.dropdown-content a:nth-child(2)').addEventListener('click', function(event) {
    event.preventDefault();
    showSystemStats();
    displayMemoryChart();
  });

  document.querySelector('.dropdown-content a:nth-child(3)').addEventListener('click', function(event) {
    event.preventDefault();
    showSystemStats();
    displayDriveChart();
  });

  // Initially show home page
  showHomePage();
});
