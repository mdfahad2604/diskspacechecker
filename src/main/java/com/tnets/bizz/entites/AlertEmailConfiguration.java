package com.tnets.bizz.entites;

import java.io.FileReader;
import java.io.IOException;

import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Component 
public class AlertEmailConfiguration {
	
	//private static final String CONFIG_FILE_PATH = "D:\\DiskSpaceChecker\\Disk-Space-Checker\\src\\main\\resources\\alert_email_config_DSC.json";
	//private static final String CONFIG_FILE_PATH = "E:\\Fahad\\email_config_DSC.json";
	private static final String CONFIG_FILE_PATH = "Z:\\APPLICATION\\DiskSpaceChecker\\alert_email_config_DSC.json";
	public JsonObject loadAlertEmailConfiguration(String string) throws IOException {
        Gson gson = new Gson();
        return JsonParser.parseReader(new FileReader(CONFIG_FILE_PATH)).getAsJsonObject();
    }
}
