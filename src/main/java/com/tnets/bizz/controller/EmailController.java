package com.tnets.bizz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tnets.bizz.service.EmailService;

@RestController
@RequestMapping("/email")

public class EmailController {
	
	@Autowired
    private EmailService emailService;

    @GetMapping("/sendNightlyReport")
    public String sendNightlyReport() {
        emailService.sendNightlyReport();
        return "Nightly report scheduled for delivery.";
    }

    @GetMapping("/triggerAlert")
    public String triggerAlert() {
        emailService.sendAlertEmail();
        return "Alert triggered.";
    }

}
