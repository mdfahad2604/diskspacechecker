package com.tnets.bizz.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.tnets.bizz.entites.AlertEmailConfiguration;
import com.tnets.bizz.entites.EmailConfiguration;
import com.tnets.bizz.util.EmailContentBuilder;
import com.tnets.bizz.util.EmailSender;
import com.tnets.bizz.util.SystemStatsGenerator;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class EmailService {

    private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

    private static final String TWILIO_ACCOUNT_SID = "ACc8478e127dc540aa05636c57296be84d";
    private static final String TWILIO_AUTH_TOKEN = "8756b9f26f642094dd8d65301a0b9451";
    private static final String DESTINATION_PHONE_NUMBER = "+919363697887";
    private static final String MESSAGING_SERVICE_SID = "MG29e0c0c82ada41b5ea76ab5ba1153404";

    static {
        Twilio.init(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);
        logger.info("Twilio API initialized with SID: {}", TWILIO_ACCOUNT_SID);
    }

    @Autowired
    private EmailConfiguration emailConfiguration;

    @Autowired
    private AlertEmailConfiguration alertEmailConfiguration;

    @Autowired
    private EmailContentBuilder contentBuilder;

    @Autowired
    private EmailSender emailSender;

    @Scheduled(cron = "0 0 08 * * *") // Daily at 8
    public void sendNightlyReport() {
        try {
            logger.info("Starting nightly report...");
            JsonObject systemStats = SystemStatsGenerator.generateSystemStats();
            String emailContent = contentBuilder.buildEmailContent(systemStats);
            if (emailContent != null && !emailContent.isEmpty()) {
                emailSender.sendEmail(emailConfiguration.loadEmailConfiguration("smtp"), emailContent);
                logger.info("Nightly report email notification sent successfully");
            } else {
                logger.error("Nightly report email content is empty. Skipping email notification.");
            }
        } catch (Exception e) {
            logger.error("Error occurred while sending nightly report email", e);
        }
    }

    @Scheduled(fixedDelay = 60000) // Every 1 minute
    public void sendAlertEmail() {
        try {
            logger.info("Checking for system alerts...");
            JsonObject systemStats = SystemStatsGenerator.generateSystemStats();
            double cpuUsage = systemStats.get("cpuUsage").getAsDouble();
            double memoryUsage = systemStats.get("memoryUsage").getAsDouble();
            boolean diskSpaceAboveThreshold = checkDiskSpaceThreshold(systemStats);

            StringBuilder alertReason = new StringBuilder();
            if (cpuUsage >= 90) {
                alertReason.append("CPU usage is high (").append(cpuUsage).append("%). ");
            }
            if (memoryUsage >= 92) {
                alertReason.append("Memory usage is high (").append(memoryUsage).append("%). ");
            }
            if (diskSpaceAboveThreshold) {
                alertReason.append("Disk space usage is above threshold. ");
            }

            if (!alertReason.toString().isEmpty()) {
                logger.info("Triggering alert due to: {}", alertReason.toString().trim());
                String emailContent = contentBuilder.alertEmailContent(systemStats);
                if (emailContent != null && !emailContent.isEmpty()) {
                    emailSender.sendEmail(alertEmailConfiguration.loadAlertEmailConfiguration("smtp"), emailContent);
                    logger.info("Alert email notification sent successfully");

                    // Send SMS using Twilio
                    sendSMS("Alert: " + alertReason.toString().trim());
                    logger.info("SMS notification sent successfully");
                } else {
                    logger.info("Generated alert email content is empty.");
                }
            } else {
                logger.info("No alerts triggered. System parameters within normal thresholds.");
            }
        } catch (Exception e) {
            logger.error("Error occurred while sending alert email or SMS", e);
        }
    }


    private void sendSMS(String message) {
    	 logger.debug("Sending SMS notification...");
        Message.creator(new PhoneNumber(DESTINATION_PHONE_NUMBER), new PhoneNumber(MESSAGING_SERVICE_SID), message).create();
        logger.info("SMS sent to {}", DESTINATION_PHONE_NUMBER);
    }

    private boolean checkDiskSpaceThreshold(JsonObject systemStats) {
        logger.info("Checking disk space threshold...");
        JsonArray diskSpaceAnalysis = systemStats.getAsJsonArray("diskSpaceAnalysis");
        for (JsonElement element : diskSpaceAnalysis) {
            JsonObject diskStats = element.getAsJsonObject();
            double usedPercentage = diskStats.get("Used Percentage").getAsDouble();
            if (usedPercentage > 90) {
                logger.info("Disk space usage is above threshold: {}%", usedPercentage);
                return true;
            }
        }
        logger.info("Disk space usage is within normal thresholds.");
        return false;
    }

}

    


