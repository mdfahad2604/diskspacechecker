package com.tnets.bizz.service;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
/*
@Service
public class TisEmailService {
    private static final String CONFIG_FILE_PATH = "/data/DSC/email_config_DSC.json";

    @Scheduled(cron = "0 * * * *") // Run every hour
    public void sendEmailNotification() {
        try {
            // Load email configuration from JSON
            Gson gson = new Gson();
            JsonObject emailConfigJson = JsonParser.parseReader(new FileReader(CONFIG_FILE_PATH)).getAsJsonObject();
            JsonObject smtpConfig = emailConfigJson.getAsJsonObject("smtp");
            JsonObject springMailConfig = emailConfigJson.getAsJsonObject("spring_mail");

            // Check disk space
            for (File root : File.listRoots()) {
                long totalSpaceGB = root.getTotalSpace() / (1024 * 1024 * 1024);
                long freeSpaceGB = root.getFreeSpace() / (1024 * 1024 * 1024);
                long usedSpaceGB = totalSpaceGB - freeSpaceGB;
                double usedPercentage = ((double) usedSpaceGB / totalSpaceGB) * 100;

                if (usedPercentage > 75) {
                    sendDiskSpaceAlert(root.getAbsolutePath(), usedPercentage, smtpConfig, springMailConfig);
                    break; // No need to check other drives if one drive is over 75%
                }
            }
        } catch (Exception e) {
            System.err.println("Error occurred while sending email notification");
            e.printStackTrace();
        }
    }

    private void sendDiskSpaceAlert(String drive, double usedPercentage, JsonObject smtpConfig, JsonObject springMailConfig) throws MessagingException {
        // Get SMTP properties
        Properties props = new Properties();
        props.put("mail.smtp.host", springMailConfig.get("host").getAsString());
        props.put("mail.smtp.port", springMailConfig.get("port").getAsInt());
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

        // Create session
        Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(springMailConfig.get("username").getAsString(), springMailConfig.get("password").getAsString());
            }
        });

        // Create MimeMessage
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(smtpConfig.get("sender").getAsString()));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(smtpConfig.get("default_to_address").getAsString()));
        message.setSubject("Disk Space Alert - " + drive);

        // Create content
        String content = "Disk space on drive " + drive + " has exceeded 75%. Used percentage: " + usedPercentage + "%";

        // Set content
        message.setText(content);

        // Send message
        Transport.send(message);

        System.out.println("Disk space alert email notification sent successfully");
    }
}

*/