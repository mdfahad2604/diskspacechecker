package com.tnets.bizz.util;

import java.io.File;
import java.util.Arrays;

import org.springframework.stereotype.Component;

@Component
public class FileUtils {
	
    public static String findLastUpdatedZipFile(String directoryPath) {
        File directory = new File(directoryPath);
        File[] files = directory.listFiles((dir, name) -> name.toLowerCase().endsWith(".zip"));
        if (files != null && files.length > 0) {
            Arrays.sort(files, (f1, f2) -> Long.compare(f2.lastModified(), f1.lastModified()));
            return files[0].getName();
        }
        return "No zip files found.";
    }

}
