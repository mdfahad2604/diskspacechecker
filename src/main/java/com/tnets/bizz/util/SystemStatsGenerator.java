package com.tnets.bizz.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sun.management.OperatingSystemMXBean;

@Component
public class SystemStatsGenerator {

	protected static final Logger logger = LoggerFactory.getLogger(SystemStatsGenerator.class);

	public static JsonObject generateSystemStats() {
		JsonObject systemStats = new JsonObject();

		// CPU and Memory Consumption
		OperatingSystemMXBean osBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);
		double cpuUsage = osBean.getSystemCpuLoad() * 100; // CPU usage in percentage
		long totalMemory = osBean.getTotalPhysicalMemorySize(); // Total memory in bytes
		long freeMemory = osBean.getFreePhysicalMemorySize(); // Free memory in bytes
		long usedMemory = totalMemory - freeMemory; // Used memory in bytes
		double memoryUsage = ((double) usedMemory / totalMemory) * 100; // Memory usage in percentage

		systemStats.addProperty("cpuUsage", cpuUsage);
		systemStats.addProperty("memoryUsage", memoryUsage);

		// Top 5 Application used Memory
		try {
			Process powerShellProcess = Runtime.getRuntime().exec(
					"powershell.exe Get-Process | Sort-Object WorkingSet -Descending | Select-Object -First 5 Name, WorkingSet | ConvertTo-Csv -NoTypeInformation");
			BufferedReader reader = new BufferedReader(new InputStreamReader(powerShellProcess.getInputStream()));

			StringBuilder topMemoryProcesses = new StringBuilder();
			String line;
			int count = 0;
			while ((line = reader.readLine()) != null && count < 5) {
				if (!line.contains("Name")) { // Skip the line containing column headers
					String[] parts = line.split(",");
					if (parts.length >= 2) {
						String name = parts[0].replaceAll("^\"", "").replaceAll("\"$", "").trim();
						long memoryUsageBytes = Long
								.parseLong(parts[1].replaceAll("^\"", "").replaceAll("\"$", "").trim());
						double memoryUsageMB = memoryUsageBytes / 1024.0 / 1024.0; // Convert bytes to megabytes
						topMemoryProcesses.append("<tr><td>").append(name).append("</td><td>")
								.append(String.format("%.2f", memoryUsageMB)).append("</td></tr>");
						count++;
					}
				}
			}
			systemStats.addProperty("topMemoryProcesses", topMemoryProcesses.toString());
		} catch (IOException e) {
			logger.error("Error occurred while fetching top memory processes", e);
		}

		//Disk Space Analysis
		JsonArray diskSpaceAnalysis = new JsonArray();
		for (File root : File.listRoots()) {
			JsonObject diskStats = new JsonObject();
			long totalSpaceGB = root.getTotalSpace() / (1024 * 1024 * 1024);
			long freeSpaceGB = root.getFreeSpace() / (1024 * 1024 * 1024);
			long usedSpaceGB = totalSpaceGB - freeSpaceGB;
			double usedPercentage = ((double) usedSpaceGB / totalSpaceGB) * 100;

			diskStats.addProperty("Drive", root.getAbsolutePath());
			diskStats.addProperty("Total Space (GB)", totalSpaceGB);
			diskStats.addProperty("Free Space (GB)", freeSpaceGB);
			diskStats.addProperty("Used Space (GB)", usedSpaceGB);
			diskStats.addProperty("Used Percentage", usedPercentage);

			diskSpaceAnalysis.add(diskStats);
		}
		systemStats.add("diskSpaceAnalysis", diskSpaceAnalysis);
		return systemStats;
	}
	
	/*
    // Disk Space Analysis
    StringBuilder diskSpaceAnalysis = new StringBuilder();
    for (File root : File.listRoots()) {
        long totalSpaceGB = root.getTotalSpace() / (1024 * 1024 * 1024);
        long freeSpaceGB = root.getFreeSpace() / (1024 * 1024 * 1024);
        long usedSpaceGB = totalSpaceGB - freeSpaceGB;
        double usedPercentage = ((double) usedSpaceGB / totalSpaceGB) * 100;
        
        String cellColor;
        if (usedPercentage <= 50) {
            cellColor = "#63ff5d"; // Light Green
        } else if (usedPercentage <= 75) {
            cellColor = "#f0ca4e"; // Light Orange
        } else {
            cellColor = "#ff5d5d"; // Red
        }

        diskSpaceAnalysis.append("<tr><td>").append(root.getAbsolutePath()).append("</td>")
                .append("<td>").append(totalSpaceGB).append(" GB</td>")
                .append("<td>").append(freeSpaceGB).append(" GB</td>")
                .append("<td>").append(usedSpaceGB).append(" GB</td>")
                .append("<td style=\"background-color:").append(cellColor).append(";\">").append(String.format("%.2f", usedPercentage)).append("%</td></tr>");
    }
    systemStats.addProperty("diskSpaceAnalysis", diskSpaceAnalysis.toString());
*/

}
