package com.tnets.bizz.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Base64;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Component
public class EmailContentBuilder {

	public String buildEmailContent(JsonObject systemStats) {
		
 /*                  
        // Create the HTML content of the message
        String lastUpdatedZipFileDMS41_DC1 = FileUtils.findLastUpdatedZipFile("D:\\PRODUCTION DATABASE\\DMS41_DC1");
        String lastUpdatedZipFileDMS41_DC2 = FileUtils.findLastUpdatedZipFile("D:\\PRODUCTION DATABASE\\DMS41_DC2");
        String lastUpdatedZipFileTICKET_TRACKER = FileUtils.findLastUpdatedZipFile("D:\\PRODUCTION DATABASE\\TICKET TRACKER");
        String lastUpdatedZipFileTNETS41_DC1 = FileUtils.findLastUpdatedZipFile("D:\\PRODUCTION DATABASE\\TNETS41_DC1");
       	String lastUpdatedZipFileTNETS41_DC2 = FileUtils.findLastUpdatedZipFile("D:\\PRODUCTION DATABASE\\TNETS41_DC2");
       	String lastUpdatedZipFileTNETS41_HQ = FileUtils.findLastUpdatedZipFile("D:\\PRODUCTION DATABASE\\TNETS41_HQ");
       	String lastUpdatedZipFileTNETSBI = FileUtils.findLastUpdatedZipFile("D:\\PRODUCTION DATABASE\\TNETSBI");
*/
	    StringBuilder contentBuilder = new StringBuilder("<html><body>");
	    try {
	        contentBuilder.append("<p><b>Hi Team,</b></p>");
	       /* contentBuilder.append("<p><b>CPU Usage:</b> ").append(String.format("%.3f%%", systemStats.get("cpuUsage").getAsDouble())).append("</p>");
	        contentBuilder.append("<p><b>Memory Usage:</b> ").append(String.format("%.3f%%", systemStats.get("memoryUsage").getAsDouble())).append("</p>");

	        contentBuilder.append("<p><b>Top 5 Processes by Memory Usage:</b></p>");
	        contentBuilder.append("<table border=\"1\"><tr><th>Process</th><th>Memory Usage (MB)</th></tr>");
	        contentBuilder.append(systemStats.get("topMemoryProcesses"));
	        contentBuilder.append("</table>");
*/
	        contentBuilder.append("<p>Please find below the latest update on Disk Space Analysis.</p>");

	        // Disk Space Analysis Table
	        contentBuilder.append("<table border=\"1\">");
	        contentBuilder.append("<tr><th>Drive</th><th>Total Space</th><th>Free Space</th><th>Used Space</th><th>Used Percentage</th></tr>");
	        JsonArray diskSpaceAnalysis = systemStats.getAsJsonArray("diskSpaceAnalysis");
	        for (int i = 0; i < diskSpaceAnalysis.size(); i++) {
	            JsonObject diskStats = diskSpaceAnalysis.get(i).getAsJsonObject();
	            double usedPercentage = diskStats.get("Used Percentage").getAsDouble();
	            String color = "";
	            if (usedPercentage < 50) {
	                color = "#63ff5d"; // green
	            } else if (usedPercentage >= 50 && usedPercentage < 75) {
	                color = "#f0ca4e"; // yellow
	            } else {
	                color = "#ff5d5d"; // red
	            }
	            contentBuilder.append("<tr>");
	            contentBuilder.append("<td>").append(diskStats.get("Drive").getAsString().replace("\"", "")).append("</td>");
	            contentBuilder.append("<td>").append(diskStats.get("Total Space (GB)")).append(" GB</td>");
	            contentBuilder.append("<td>").append(diskStats.get("Free Space (GB)")).append(" GB</td>");
	            contentBuilder.append("<td>").append(diskStats.get("Used Space (GB)")).append(" GB</td>");
	            contentBuilder.append("<td style=\"background-color:").append(color).append(";\">")
	                    .append(String.format("%.2f%%", usedPercentage)).append("</td>");
	            contentBuilder.append("</tr>");
	        }
	        contentBuilder.append("</table>");

	        // Color Code Legend
	        contentBuilder.append("<table border=\"0\" style=\"font-size: smaller;\">");
	        contentBuilder.append("<tr><td style=\"background-color:#63ff5d;width:10px;height:5px;\"></td><td>Below 50%</td>");
	        contentBuilder.append("<td style=\"background-color:#f0ca4e;width:10px;height:5px;\"></td><td>50% to 75%</td>");
	        contentBuilder.append("<td style=\"background-color:#ff5d5d;width:10px;height:5px;\"></td><td>Above 75%</td></tr>");
	        contentBuilder.append("</table>");

	        // Disk Space Analysis Chart
	        contentBuilder.append("<p><b>Disk Space Analysis Chart:</b></p>");
	        contentBuilder.append("<div style=\"display:flex;\">");

	        for (File root : File.listRoots()) {
	            DefaultPieDataset dataset = new DefaultPieDataset();
	            long totalSpaceGB = root.getTotalSpace() / (1024 * 1024 * 1024);
	            long freeSpaceGB = root.getFreeSpace() / (1024 * 1024 * 1024);
	            long usedSpaceGB = totalSpaceGB - freeSpaceGB;

	            dataset.setValue("Used (" + usedSpaceGB + "GB)", usedSpaceGB);
	            dataset.setValue("Free (" + freeSpaceGB + "GB)", freeSpaceGB);

	            JFreeChart chart = ChartFactory.createPieChart(root.getAbsolutePath(), dataset, true, true, false);

	            ByteArrayOutputStream chartOut = new ByteArrayOutputStream();
	            ChartUtils.writeChartAsPNG(chartOut, chart, 300, 200);
	            byte[] chartBytes = chartOut.toByteArray();
	            String chartBase64 = Base64.getEncoder().encodeToString(chartBytes);

	            contentBuilder.append("<div style=\"margin-right: 20px;\">")
	                    .append("<img src=\"data:image/png;base64,").append(chartBase64).append("\" alt=\"Pie Chart\">")
	                    .append("</div>");
	        }

	        contentBuilder.append("</div>");
/*	        
	        //Backup Production DB files details from Server
            contentBuilder.append("<tr>")
            .append("<p>Backup Production DB files details from Server .,</p>")
            .append("DC1: DMS41 - ").append(lastUpdatedZipFileDMS41_DC1).append("<br>")
            .append("DC1: TNETS41 - ").append(lastUpdatedZipFileTNETS41_DC1).append("<br><br>")
            .append("DC2: DMS41 - ").append(lastUpdatedZipFileDMS41_DC2).append("<br>")
            .append("DC2: TNETS41 - ").append(lastUpdatedZipFileTNETS41_DC2).append("<br><br>")
            .append("HQ: TNETS41 - ").append(lastUpdatedZipFileTNETS41_HQ).append("<br><br>")
            .append("BI: ").append(lastUpdatedZipFileTNETSBI).append("<br>")
            .append("TT360: ").append(lastUpdatedZipFileTICKET_TRACKER).append("<br>")
            .append("<br><br>")
            .append("</tr>");
*/
	        // Closing Tags
	        contentBuilder.append("<br>");
	        contentBuilder.append("<br>Thanks and Regards,</br>");
	        contentBuilder.append("DSA Administrator<br>");
	        contentBuilder.append("IT Team<br>");
	        contentBuilder.append("<p><b>Note:</b> This email has been auto-generated from the Bizz Customer Support. <b>Please do not reply to this auto-generated email.</b></p>");
	        contentBuilder.append("</body></html>");

	    } catch (Exception e) {
	        System.err.println("Error occurred while building email content");
	        e.printStackTrace();
	    }
	    return contentBuilder.toString();
	}
    public String alertEmailContent(JsonObject systemStats) {
        StringBuilder alertEmailContentBuilder = new StringBuilder("<html><body>");
        try {
            alertEmailContentBuilder.append("<p><b>Hi Team,</b></p>");
            alertEmailContentBuilder.append("<p>This Email is for Alert </p>");
            alertEmailContentBuilder.append("<p><b>CPU Usage:</b> ").append(String.format("%.3f%%", systemStats.get("cpuUsage").getAsDouble())).append("</p>");
	        alertEmailContentBuilder.append("<p><b>Memory Usage:</b> ").append(String.format("%.3f%%", systemStats.get("memoryUsage").getAsDouble())).append("</p>");

            alertEmailContentBuilder.append("<p><b>Top 5 Processes by Memory Usage:</b></p>");
            alertEmailContentBuilder.append("<table border=\"1\"><tr><th>Process</th><th>Memory Usage (MB)</th></tr>");
            alertEmailContentBuilder.append(systemStats.get("topMemoryProcesses"));
            alertEmailContentBuilder.append("</table>");

            alertEmailContentBuilder.append("<p>Please find below the latest update on Disk Space Analysis.</p>");

	        // Disk Space Analysis Table
            alertEmailContentBuilder.append("<table border=\"1\">");
	        alertEmailContentBuilder.append("<tr><th>Drive</th><th>Total Space</th><th>Free Space</th><th>Used Space</th><th>Used Percentage</th></tr>");
	        JsonArray diskSpaceAnalysis1 = systemStats.getAsJsonArray("diskSpaceAnalysis");
	        for (int i = 0; i < diskSpaceAnalysis1.size(); i++) {
	            JsonObject diskStats = diskSpaceAnalysis1.get(i).getAsJsonObject();
	            double usedPercentage = diskStats.get("Used Percentage").getAsDouble();
	            String color = "";
	            if (usedPercentage < 50) {
	                color = "#63ff5d"; // green
	            } else if (usedPercentage >= 50 && usedPercentage < 75) {
	                color = "#f0ca4e"; // yellow
	            } else {
	                color = "#ff5d5d"; // red
	            }
	            alertEmailContentBuilder.append("<tr>");
	            alertEmailContentBuilder.append("<td>").append(diskStats.get("Drive").getAsString().replace("\"", "")).append("</td>");
	            alertEmailContentBuilder.append("<td>").append(diskStats.get("Total Space (GB)")).append(" GB</td>");
	            alertEmailContentBuilder.append("<td>").append(diskStats.get("Free Space (GB)")).append(" GB</td>");
	            alertEmailContentBuilder.append("<td>").append(diskStats.get("Used Space (GB)")).append(" GB</td>");
	            alertEmailContentBuilder.append("<td style=\"background-color:").append(color).append(";\">")
	                    .append(String.format("%.2f%%", usedPercentage)).append("</td>");
	            alertEmailContentBuilder.append("</tr>");
	        }
	        alertEmailContentBuilder.append("</table>");

            // Color Code Legend
            alertEmailContentBuilder.append("<table border=\"0\" style=\"font-size: smaller;\">");
            alertEmailContentBuilder.append("<tr><td style=\"background-color:#63ff5d;width:10px;height:5px;\"></td><td>Below 50%</td>");
            alertEmailContentBuilder.append("<td style=\"background-color:#f0ca4e;width:10px;height:5px;\"></td><td>50% to 75%</td>");
            alertEmailContentBuilder.append("<td style=\"background-color:#ff5d5d;width:10px;height:5px;\"></td><td>Above 75%</td></tr>");
            alertEmailContentBuilder.append("</table>");

            // Disk Space Analysis Chart
            alertEmailContentBuilder.append("<p><b>Disk Space Analysis Chart:</b></p>");
            alertEmailContentBuilder.append("<div style=\"display:flex;\">");

            for (File root : File.listRoots()) {
                DefaultPieDataset dataset = new DefaultPieDataset();
                long totalSpaceGB = root.getTotalSpace() / (1024 * 1024 * 1024);
                long freeSpaceGB = root.getFreeSpace() / (1024 * 1024 * 1024);
                long usedSpaceGB = totalSpaceGB - freeSpaceGB;

                dataset.setValue("Used (" + usedSpaceGB + "GB)", usedSpaceGB);
                dataset.setValue("Free (" + freeSpaceGB + "GB)", freeSpaceGB);

                JFreeChart chart = ChartFactory.createPieChart(root.getAbsolutePath(), dataset, true, true, false);

                ByteArrayOutputStream chartOut = new ByteArrayOutputStream();
                ChartUtils.writeChartAsPNG(chartOut, chart, 300, 200);
                byte[] chartBytes = chartOut.toByteArray();
                String chartBase64 = Base64.getEncoder().encodeToString(chartBytes);

                alertEmailContentBuilder.append("<div style=\"margin-right: 20px;\">")
                        .append("<img src=\"data:image/png;base64,").append(chartBase64).append("\" alt=\"Pie Chart\">")
                        .append("</div>");
            }

            alertEmailContentBuilder.append("</div>");

            // Closing Tags
            alertEmailContentBuilder.append("<br>");
            alertEmailContentBuilder.append("<br>Thanks and Regards,</br>");
            alertEmailContentBuilder.append("DSA Administrator<br>");
            alertEmailContentBuilder.append("IT Team<br>");
            alertEmailContentBuilder.append("<p><b>Note:</b> This email has been auto-generated from the Bizz Customer Support. <b>Please do not reply to this auto-generated email.</b></p>");
            alertEmailContentBuilder.append("</body></html>");

        } catch (Exception e) {
            System.err.println("Error occurred while building email content");
            e.printStackTrace();
        }
        return alertEmailContentBuilder.toString();
    }   
}
