package com.tnets.bizz.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Component
public class EmailSender {
	public void sendEmail(JsonObject emailConfigJson, String emailContent) throws MessagingException {

		// Load email configuration from JSON file
		JsonObject smtpConfig = emailConfigJson.getAsJsonObject("smtp");
		JsonObject springMailConfig = emailConfigJson.getAsJsonObject("spring_mail");

		// Create SMTP properties
		Properties props = new Properties();
		props.put("mail.smtp.host", springMailConfig.get("host").getAsString());
		props.put("mail.smtp.port", springMailConfig.get("port").getAsInt());
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.user", springMailConfig.get("username").getAsString());
		props.put("mail.smtp.password", springMailConfig.get("password").getAsString());

		// Create session
		Session session = Session.getInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(springMailConfig.get("username").getAsString(),
						springMailConfig.get("password").getAsString());
			}
		});

		// Get copy recipients
		JsonArray copyRecipientsArray = smtpConfig.getAsJsonArray("copy_recipients");
		List<String> copyRecipientsList = new ArrayList<>();
		for (JsonElement element : copyRecipientsArray) {
			copyRecipientsList.add(element.getAsString());
		}
		// Get BCC recipients
		JsonArray bccRecipientsArray = smtpConfig.getAsJsonArray("bcc_recipients");
		List<String> bccRecipientsList = new ArrayList<>();
		for (JsonElement element : bccRecipientsArray) {
			bccRecipientsList.add(element.getAsString());
		}

		// Create message
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(smtpConfig.get("sender").getAsString()));
		message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(smtpConfig.get("default_to_address").getAsString()));
		message.setSubject("HQ_" + smtpConfig.get("subject").getAsString());

		// Adding CC recipients
		for (String ccRecipient : copyRecipientsList) {
			message.addRecipient(Message.RecipientType.CC, new InternetAddress(ccRecipient));
		}

		// Adding BCC recipients
		for (String bccRecipient : bccRecipientsList) {
			message.addRecipient(Message.RecipientType.BCC, new InternetAddress(bccRecipient));
		}
		message.setContent(emailContent, "text/html");
		
		// Send message
		Transport.send(message);

	}
}
